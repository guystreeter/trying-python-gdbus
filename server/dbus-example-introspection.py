#!/usr/bin/env python3

# Copyright (C) 2015 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <streeter@redhat.com>

from gi.repository import GObject, Gio, GLib

from common import BUS_NAME, dump_args, INTERFACE_NAME, OBJECT_PATH
import sys, os, time


main_loop = GObject.MainLoop()

def get_introspection(connection, name_owner):
    method_call_message = Gio.DBusMessage.new_method_call(name_owner,
                                                          OBJECT_PATH,
                                                          'org.freedesktop.DBus.Introspectable',
                                                          'Introspect')
    method_reply_message = connection.send_message_with_reply_sync(method_call_message,
                                                                               Gio.DBusSendMessageFlags.NONE,
                                                                               -1, None)[0]
    print(method_reply_message)
    try:
        # Raises an exception if there is an error. Does nothing otherwise
        method_reply_message.to_gerror()
    except GLib.Error as err:
        # can I do something useful with this?
        #print(method_reply_message.get_error_name())
        raise err
    return method_reply_message.get_body().unpack()[0]

def print_annotations(annotations, indent=2):
    if annotations:
        print(' '*(indent-1), 'annotations:')
    for ann in annotations:
        print(' '*indent, ann.key, ann.value)
        print_annotations(ann.annotations, indent+4)

def print_interface_info(interface_info, indent=2):
    print(' '*indent, 'interface', interface_info.name)
    interface_info.cache_build()
    print_annotations(interface_info.annotations, indent+2)
    for meth in interface_info.methods:
        print(' '*indent, 'method:', meth.name)
        print_annotations(meth.annotations)
        for inargs in meth.in_args:
            print(' '*indent, 'in:', inargs.name, inargs.signature)
            print_annotations(inargs.annotations, indent+2)
        for outargs in meth.out_args:
            print(' '*indent, 'out:', outargs.name, outargs.signature)
            print_annotations(outargs.annotations, indent+2)
    for prop in interface_info.properties:
        print(' '*indent, 'property:', prop.name, prop.signature, prop.flags)
        print_annotations(prop.annotations, indent+2)

def print_node_info(node_info, indent=0):
    print(' '*indent, 'node path', node_info.path)
    print_annotations(node_info.annotations, 0)
    for iinfo in node_info.interfaces:
        print_interface_info(iinfo, indent)
    for ninfo in node_info.nodes:
        print_node_info(ninfo, indent+1)

@dump_args
def name_appeared_handler(connection, name, name_owner):
    introspection_xml = get_introspection(connection, name_owner)
    introspection_data = Gio.DBusNodeInfo.new_for_xml(introspection_xml)
    print(introspection_data)
    print_node_info(introspection_data)
    main_loop.quit()

@dump_args
def name_vanished_handler(connection, name):
    print('Failed to get name owner for', name,
          'Is ./dbus-example-server.py running?',
          file=sys.stderr)
    main_loop.quit()

watcher_id = Gio.bus_watch_name(Gio.BusType.SESSION, BUS_NAME,
                                Gio.BusNameWatcherFlags.NONE,
                                name_appeared_handler,
                                name_vanished_handler)

main_loop.run()

Gio.bus_unwatch_name(watcher_id)
