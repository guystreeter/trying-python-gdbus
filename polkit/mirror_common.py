OBJECT_PATH = '/com/example/Mirror'

# The bus name is what the server advertises and the client requests,
# to initially connect to each other. The com.example.Mirror.service
# file tells DBus where to find and start the server if it isn't running.
# for that to work, we would need to install the mirror-server.py file
# in the specified location. 
BUS_NAME = 'com.example.Mirror'

# This matches the interface name in our introspection XML data. The
# server could support multiple interfaces. It would probably be more
# clear if we used text that was different from the bus name.
INTERFACE_NAME = 'com.example.Mirror'

# This is the action we request polkit to authorize us for. It is
# specified in the mirror.policy file, installed in the polkit policy
# directory
INTERFACE_ACTION = 'com.example.mirror.use'


def dump_args(func):
    """
    Using for debugging...
    """
    def wrapper(*func_args, **func_kwargs):
        arg_names = func.__code__.co_varnames[:func.__code__.co_argcount]
        args = func_args[:len(arg_names)]
        defaults = func.__defaults__ or ()
        args = args + defaults[len(defaults) -
                               (func.__code__.co_argcount - len(args)):]
        params = list(zip(arg_names, args))
        args = func_args[len(arg_names):]
        if args:
            params.append(('args', args))
        if func_kwargs:
            params.append(('kwargs', func_kwargs))
        print(func.__name__ +
              ' (' + ', '.join('%s = %r' % p for p in params) + ' )')
        return func(*func_args, **func_kwargs)
    return wrapper
