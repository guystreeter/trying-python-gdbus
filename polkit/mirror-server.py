#!/usr/bin/env python3

# https://github.com/herzi/polkit-dbus-example/blob/master/mirror-server.c

from gi import require_version as gi_require_version
gi_require_version('Polkit', '1.0')

from gi.repository import Gio, GObject, Polkit, GLib
from mirror_common import dump_args, INTERFACE_NAME, OBJECT_PATH, \
 BUS_NAME, INTERFACE_ACTION

introspection_xml = """
<?xml version="1.0" encoding="UTF-8" ?>
<node name="/com/example/Mirror">
 <interface name="com.example.Mirror">
  <method name="Reflect">
   <annotation name="org.freedesktop.DBus.GLib.Async" value=""/>
   <arg type="s" name="in" direction="in" />
   <arg type="s" name="out" direction="out" />
  </method>
 </interface>
</node>
"""
introspection_data = Gio.DBusNodeInfo.new_for_xml(introspection_xml)
interface_info = introspection_data.lookup_interface(INTERFACE_NAME)

main_loop = GObject.MainLoop()

@dump_args
def handle_method_call(connection, sender, object_path, interface_name,
                       method_name, parameters, invocation):
    assert method_name == 'Reflect'
    authority = Polkit.Authority.get_sync()
    bus_name = Polkit.SystemBusName.new(sender)
    result = authority.check_authorization_sync(bus_name,
                                                INTERFACE_ACTION, None,
                                                Polkit.CheckAuthorizationFlags.ALLOW_USER_INTERACTION)
    if not result.get_is_authorized():
        print('Not authorized')
        response = "Not authorized"
        invocation.return_error_literal(Gio.dbus_error_quark(),
                                             Gio.DBusError.ACCESS_DENIED,
                                             response)
        return
    string, = parameters.unpack()
    string = string[::-1]
    invocation.return_value(GLib.Variant('(s)', (string,)))

def bus_acquired_handler(connection, name):
    print('bus acquired', name, connection.is_closed())
    registration_id = connection.register_object(OBJECT_PATH,
                                                 interface_info,
                                                 handle_method_call,
                                                 None,
                                                 None)
    assert registration_id > 0

def name_acquired_handler(connection, name):
    print('name acquired', name)

def name_lost_handler(connection, name):
    print('name lost', name)
    main_loop.quit()

owner_id = Gio.bus_own_name(Gio.BusType.SYSTEM,
                            BUS_NAME,
                            Gio.BusNameOwnerFlags.NONE,
                            bus_acquired_handler,
                            name_acquired_handler,
                            name_lost_handler)
assert owner_id
main_loop.run()
Gio.bus_unown_name(owner_id)

