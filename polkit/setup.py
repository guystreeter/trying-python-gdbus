#!/usr/bin/python3
#
# Copyright (C) 2015 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <streeter@redhat.com>
#

from distutils.core import setup
#from distutils.command.install import install
from distutils.core import Command
import os

__package_name = 'mirror-server'
__author = 'Guy Streeter'
__author_email = '<streeter@redhat.com>'
__license = 'GPLv2+'
__description = 'DBus and PolKit example in Python'
__version = '0.1'
__url = ''

#install.sub_commands.append(('install_dbus_service',))

datadir = os.environ.get('DATADIR', 'share')

configdir = os.environ.get('CONFIGDIR', '/etc')

try:
    docdir = os.environ['LICENSEDIR']
except KeyError:
    docdir = os.path.join(datadir, 'doc', __package_name)

dbus_datadir = os.path.join(datadir, 'dbus-1', 'system-services')
dbus_configdir = os.path.join(configdir, 'dbus-1', 'system.d')
polkitdir = os.path.join(datadir, 'polkit-1', 'actions')

data_files = [
              (dbus_datadir, ['com.example.Mirror.service']),
              (dbus_configdir, ['mirror.conf']),
              (polkitdir, ['mirror.policy'])
              ]

setup(name=__package_name,
      version = __version,
      description = __description,
      author = __author,
      author_email = __author_email,
      url = __url,
      license = __license,
      long_description = __description,
      data_files = data_files,
#      cmdclass = {'install_bus_service' : install_dbus_service,
#                },
      )
