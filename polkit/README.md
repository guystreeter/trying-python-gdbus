The configuration files actually have to be installed for this demo to work. The `setup.py` command only installs those files. Just run the client and server from the local directory.

`distutils` doesn't support un-install, but if you install like this

~~~
sudo ./setup.py install --record=install.log
~~~

then you will have in the `install.log` file a list of the installed files you
can manually remove later.

Run `./mirror-server.py` in one terminal and `./mirror-client.py` in another.
